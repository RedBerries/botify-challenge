let nextCommentId = 0
export const addComment = (content, user, email) => {
	    var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		var hh = today.getHours();
		var xx = today.getMinutes();
		var ss = today.getSeconds();

		if(dd<10) {
		    dd='0'+dd
		} 

		if(mm<10) {
		    mm='0'+mm
		} 

		today = hh+':'+xx+':'+ss+' '+dd+'/'+mm+'/'+yyyy;

  return {
    type: 'ADD_COMMENT',
    id: nextCommentId++,
    content,
    user,
    email,
    time: today
  }
}