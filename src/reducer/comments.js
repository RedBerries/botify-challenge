const comment = (state = {}, action) => {
  return {
    id: action.id,
    content: action.content,
    user: action.user,
    email: action.email,
    time: action.time
  }
}

const comments = (state = [], action) => {
  switch (action.type) {
    case 'ADD_COMMENT':
      return [
        ...state,
        comment(undefined, action)
      ]
    default:
      return state
  }
}

export default comments