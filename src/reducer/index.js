import { combineReducers } from 'redux'
import comments from './comments'

const todoApp = combineReducers({
  comments
})

export default todoApp