import React from 'react'
import AddComment from '../containers/AddComment'
import GetCommentList from '../containers/GetCommentList'

const App = () => (
  <div>
  	<img src='http://lorempixel.com/640/480/animals' />
  	<h2>Please write a comment</h2>
    <AddComment />
    <GetCommentList />
  </div>
)

export default App