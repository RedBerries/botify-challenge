import React, { PropTypes } from 'react'
import Comment from './Comment'

const CommentList = ({ comments }) => (
  <ul>
    {comments.map(comment =>
      <Comment
        key={comment.id}
        {...comment}
      />
    )}
  </ul>
)

CommentList.propTypes = {
  comments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    email: PropTypes.string,
    time: PropTypes.string.isRequired
  }).isRequired).isRequired
}

export default CommentList