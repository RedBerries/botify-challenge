import React, { PropTypes } from 'react'

const Comment = ({ id, time, content, user, email }) => (
  <div>
    MY ID: {id}<br />
    MY TIMESTAMP: {time}<br />
    MY CONTENT: {content}<br />
    MY USER: {user}<br />
    MY EMAIL: {email}<hr />
  </div>
)

Comment.propTypes = {
  content: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  email: PropTypes.string
}

export default Comment