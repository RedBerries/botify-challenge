import React from 'react'
import { connect } from 'react-redux'
import { addComment } from '../actions'

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

let AddComment = ({ dispatch }) => {
  let input1
  let input2
  let input3

  return (
    <div>
      <form onSubmit={e => {
        e.preventDefault()
        if (!input1.value.trim()) {
          return
        }
        if (!input2.value.trim()) {
          return
        }
        if (input3.value != "") {
          if (validateEmail(input3.value) == false) {
            alert("Invalid email");
            return
          }
        }
        dispatch(addComment(input1.value,input2.value,input3.value))
        input1.value = ''
        input2.value = ''
        input3.value = ''
      }}>
        <input placeholder="Your comment" ref={node1 => {
          input1 = node1
        }} />
        <input placeholder="Your username" ref={node2 => {
          input2 = node2
        }} />
        <input placeholder="Your email" ref={node3 => {
          input3 = node3
        }} />
        <button type="submit">
          Add Comment
        </button>
      </form>
    </div>
  )
}
AddComment = connect()(AddComment)

export default AddComment