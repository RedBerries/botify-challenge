import { connect } from 'react-redux'
import CommentList from '../components/CommentList'

const getComments = (comments) => {
  	return comments
}

const mapStateToProps = (state) => {
  return {
    comments: getComments(state.comments)
  }
}

const GetCommentList = connect(mapStateToProps)(CommentList)

export default GetCommentList