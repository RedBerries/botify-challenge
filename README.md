# README #

### What is this repository for? ###

* Step 1 of the Botify Challenge by Jeremy LOCH

### The Challenge ###

**Goal.**
The goal is to create a comment app (almost shippable). The test consists of 3 steps.

**Source code management and hosting**
You should use Git to manage your code using iterative commits. Each step is a new branch.
Either send us your project by email or push your local git repository on a private github/bitbucket repo and send us the link!

**Step 1: Client App**
Create a Single Page Application with only one view that shows the comments list and displays a form to create a new comment.
For now, comments are stored in the localStorage of the browser.
A comment contains the following data:
username (required)
email (not required - if provided you need to validate the format)
content (required)
creationDate (the user does not fill this field himself, you have to set it when saving the comment)

*Libraries*
You will use the following libraries, which are parts of the new JS stack used at Botify:
Views and Components: React
App Structure: Redux
ES6 Javascript Compiler: Babel

*Tips*
You can use a starter kit to bootstrap your project. react-transform-boilerplate is a good one to start from.
Split your code (actions, views, components, utils..) into different files and folders.
Feel free to use any additional libraries. We are always interested in new ideas.


### How do I get set up? ###

* Clone the repository
* Navigate to project directory
* Command: npm run serve

### Sources ###

* https://school.scotch.io/getting-started-with-react-and-redux/introduction
* http://redux.js.org/docs/basics/UsageWithReact.html
* http://egorsmirnov.me/2015/05/22/react-and-es6-part1.html

### Who do I talk to? ###

* jere.loch@gmail.com